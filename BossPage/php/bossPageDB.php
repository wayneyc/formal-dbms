<?php
session_start();

// Html input
$ID = $_POST['currID'];

// Connect to DB
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dbms_test";

// Json class
class boss {
    public $status = 0;
    public $bossID = '';
    public $name = '';
    public $phone = '';
    public $email = '';
}

// Connect
$conn = new mysqli($servername, $username, $password, $dbname);

// Check
if ($conn->connect_error) {
    die("Fail: " . $conn->connect_error . "<br>");
}

// Sql
$sql = "
SELECT P.Name as name, P.Phone as phone, P.Email as email, B.BossID as bossID
FROM People as P, Bosses as B
WHERE P.ID = B.ID and P.ID = $ID
";
$result = $conn->query($sql);

// Collect data
$bossInfo = new boss();
if ($result->num_rows > 0) {
    // Success
    $row = $result->fetch_assoc();
    $bossInfo->bossID = $row["bossID"];
    $bossInfo->name = $row["name"];
    $bossInfo->phone = $row["phone"];
    $bossInfo->email = $row["email"];
    // Save ID
    $_SESSION['BossID'] = $row["bossID"];
} else {
    // Error
    $bossInfo->status = 1;
}

// Return
echo json_encode($bossInfo);

$conn->close();