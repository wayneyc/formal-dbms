<?php
session_start();
$type = $_SESSION['Type'];
$ID = $_SESSION['ID'];
?>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="css/stylecss.css">
    <script src="../myAjax.js"></script>
    <script type="text/javascript" src="../jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <script>
        $(document).ready(function() {
            // $("button").click(function(){
            // console.log("compID: " + $("#compID").val());
            // url?key:value&key:value...
            //前传后
            var ID = document.getElementById('currID').innerText;
            console.log('ID: ' + ID);
            ajax("POST", "php/bossPageDB.php", { // Input data
                    "currID": ID
                    // "currID":30001
                }, 1000 // Wait times
                //接收数据
                ,
                function(xhr) { // If success
                    var str = xhr.responseText;
                    console.log(str);
                    var obj = JSON.parse(str);
                    $("#show").append("<div class='panel panel-default'style='margin:0 auto;width:300px'>" +
                        "<div class='panel-heading'>Boos Information</div>" +
                        "<table class='table' border='1'><tr>" +
                        // "<td>Boss ID: </td>" + "<td>"+ obj.bossID + "</td>"+"</tr>"+
                        "<tr><td>Boss name:</td><td>" + obj.name + "</td></tr>" +
                        "<tr><td>Phone:</td><td>" + obj.phone + "</td></tr>" +
                        "<tr><td>Email:</td><td>" + obj.email + "</td></tr>" +
                        // "<tr><td>Company:</td><td>"+obj.compID+"</td></tr>"+
                        "</table></div>"
                    );
                },
                function(xhr) { // If fail
                    alert(xhr.status);
                })
        });
    </script>

    <div style="text-align: right" >
        <ul>
            <li><a href="php/cleanSession.php">Log Out</a></li>
            <li><a href="javascript:history.go(-1);">Back</a></li>
            <li><a href="../Mainpage/index.php">Home</a></li>
        </ul>
    </div>
</head>

<body>
<p id="currID" hidden><?php echo $_SESSION['ID'] ?>
</p>
<h1>Boss Page</h1>
<div id="infoForm">
    <h3>Boss Info</h3>
    <form id="showForm" method="post">
        <div id="show"></div>
    </form>
</div>
<div id="createCom">
    <h3>Register a new company</h3>
    <input type="button" id="showCreate" value="Create">
    <form id="newCompForm" method="post" action="php/createCompDB.php" style="display: none">
        <p>
            Company Name:
            <input type="text" name="name" id="name" />
        </p>
        <p>
            Juridical:
            <input type="text" name="juridical" id="juridical" />
        </p>
        <p>
            Capital:
            <input type="number" name="capital" id="capital" />
        </p>
        <p>
            Company Email:
            <input type="email" name="compmail" id="compmail" />
        </p>
        <p>
            City:
            <input type="text" name="city" id="city" />
        </p>
        <p>
            Company Key:
            <input type="password" name="key" id="key" />
        </p>
        <input type="submit" id="createBtn" value="Create" disabled>
        <input type="button" id="hide" value="Collapse">
    </form>

</div>

<div id="checkComp">
    <h3>Check your company</h3>
    <form id="checkCompForm" method="post">
        <input type="button" id="postBtn" onclick="window.location.href='Pagination/checkComp.php'" value="Check">
    </form>
</div>
<script>
    document.getElementById("showCreate").addEventListener("click", function() {
        showForm();
    });
    document.getElementById("hide").addEventListener("click", function() {
        hideForm();
    });
    document.getElementById("name").addEventListener("input", checkNull);
    document.getElementById("juridical").addEventListener("input", checkNull);
    document.getElementById("capital").addEventListener("input", checkNull);
    document.getElementById("compmail").addEventListener("input", checkNull);
    document.getElementById("city").addEventListener("input", checkNull);
    document.getElementById("key").addEventListener("input", checkNull);

    function showForm() {
        document.getElementById("showCreate").style.display = 'none';
        document.getElementById("newCompForm").style.display = '';
    }

    function hideForm() {
        document.getElementById("showCreate").style.display = '';
        document.getElementById("newCompForm").style.display = 'none';
    }

    function checkNull() {
        var jur = document.getElementById("juridical").value;
        var cap = document.getElementById("capital").value;
        var mail = document.getElementById("compmail").value;
        var city = document.getElementById("city").value;
        var key = document.getElementById("key").value;
        var name = document.getElementById("name").value;
        if ((jur != "") && (cap != "") && (mail != "") && (city != "") && (key != "") && (name != "")) {
            document.getElementById("createBtn").disabled = false;
        } else {
            document.getElementById("createBtn").disabled = true;
        }
    }
</script>
</body>

</html>