<?php
session_start();
$ID = $_SESSION['ID'];
$type = $_SESSION['Type'];
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/stylecss.css">
    <script src="../myAjax.js"></script>
    <script type="text/javascript" src="../jquery-3.6.0.min.js"></script>
    <title>users homepage</title>
    <script>
        $(document).ready(function(){
            var userID = document.getElementById('theID').innerText;
            console.log(userID);
            // console.log("name: " + $("#name").val());
            // url?key:value&key:value...
            ajax("POST","php/userPageDB.php", // DBfile name
                { // Input data
                    // "name":$("#name").val(),
                    "id":userID
                }, 1000 // Wait times
                ,function (xhr) { // If success
                    var str = xhr.responseText;
                    console.log(str);
                    var obj = JSON.parse(str);
                    console.log(obj.name);
                    console.log(obj.date);
                    $("#mainTable").append('<table id="userTable" style="border: 1px;margin: 0 auto;background: snow" width="50%" cellpadding="5" cellspacing="0">' +
                        "<tr>"+
                        "<td>name: </td>" + "<td>"+ obj.name + "</td>"+"</tr>"+
                        "<tr><td>gender:</td><td>" + obj.gender + "</td></tr>"+
                        "<tr><td>birthday:</td><td>"+obj.birthday+"</td></tr>"+
                        "<tr><td>education:</td><td>"+ obj.education+ "</td></tr>"+
                        "<tr><td>phone:</td><td>"+obj.phone+"</td></tr>"+
                        "<tr><td>email</td><td>"+ obj.email+"</td></tr>"
                        + '</table>');
                }, function (xhr) { // If fail
                    alert(xhr.status);
                })
        });
    </script>
</head>

<body style="text-align: center;">
<p id="theID" hidden><?php echo $ID ?></p>
<div>
<h1>users mainpage</h1>
</div>
<br>
<button id="postBtn" onclick="window.location.href='../MainPage/index.php'" value="Back">
Back </button>
<br>
<div>
    <h2>information show</h2>
    <br>
    <div id="mainTable">
    </div>
</div>
<br>
<div>
    <a href="change.php"><button name="change"/> change information </button> </a><br>
    <a href="cv.php"><button value="" name="cv"/> write your CV</button> </a><br>
    <a href="changepassword.php"><button name="changepassword"/> change your password </button> </a>
</div>
</body>
</html>