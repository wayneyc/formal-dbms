<?php

// Html input
$ID =  $_POST['id'];

// Connect to DB
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dbms_test";

// Json class
class user {
    public $status = 0;
    public $name = '';
    public $gender = '';
    public $birthday = '';
    public $education = '';
    public $phone = '';
    public $email = '';
}
// Connect
$conn = new mysqli($servername, $username, $password, $dbname);

// Check
if ($conn->connect_error) {
    die($conn->connect_error);
}

// SQL searching
$sql = "
SELECT P.Name as Name,
CASE
    P.Sex
    WHEN '0' THEN 
    'Male'
    WHEN '1' THEN 
    'Female'
END as Sex, 
    U.Birthday as Birthday, 
    U.Education as Education, 
    P.Phone as Phone, 
    P.Email as Email
FROM People as P, Users as U
WHERE $ID = P.ID and P.ID = U.ID
";
$result = $conn->query($sql);

// Collect data
$userInfo = new user();
if ($result->num_rows > 0) {
    // Success
    $row = $result->fetch_assoc();
    $userInfo->name = $row["Name"];
    $userInfo->gender = $row["Sex"];
    $userInfo->birthday = $row["Birthday"];
    $userInfo->education = $row["Education"];
    $userInfo->phone = $row["Phone"];
    $userInfo->email = $row["Email"];
} else {
    // Error
    $userInfo->status = 1;
}

// Return
echo json_encode($userInfo);

$conn->close();