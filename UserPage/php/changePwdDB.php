<?php
session_start();

// Html input
$ID = $_SESSION['ID'];
$newPwd = $_POST['newpassword'];

// Connect to DB
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dbms_test";

// Connect
$conn = new mysqli($servername, $username, $password, $dbname);

// Check
if ($conn->connect_error) {
    die("Fail: " . $conn->connect_error . "<br>");
}

// Sql
$sql = "
UPDATE People
SET Password = '$newPwd'
WHERE ID = $ID
";
$result = $conn->query($sql);

if ($result === TRUE) {
    echo "Successful! After 1 second will jump to..." . "<br>";
    echo "<meta http-equiv='Refresh' content='1; URL=../mainpage.php' > ";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}