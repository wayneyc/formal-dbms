<?php
session_start();
$type = $_SESSION['Type'];
$ID = $_SESSION['ID'];
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/stylecss.css">
    <div id="status" style="text-align: right">
        <ul>
            <li><a href="php/cleanSession.php">Log Out</a></li>
            <li><a href="javascript:history.go(-1);">Back</a></li>
            <li><a href="../Mainpage/index.php">Home</a></li>
        </ul>
    </div>
    <h1>Change Password</h1>
</head>
<body>
<form id="changePsw" action="php/modifyPsw.php" method="post">
<label>Old Password</label>
<input type="password" id="oldPsw" name="oldPsw" placeholder="old password"/>
<br><br>
<label>New Password</label>
<input type="password" id="newPsw" placeholder="new password"/>
<br><br>
<label>Confirm New Password</label>
<input type="password" id="conPsw" name="conPsw" placeholder="confirm password"/>
<br><br>
<input type="submit" value="Confirm" id="changePswBtn_con" disabled/>
</form>

<script type="text/javascript">
    document.getElementById("conPsw").addEventListener("input",checkpassword);

    function checkpassword() {
        var password = document.getElementById("newPsw").value;
        var repassword = document.getElementById("conPsw").value;

        if(password !== repassword) {
            document.getElementById("changePswBtn_con").disabled = true;
        }else {
            document.getElementById("changePswBtn_con").disabled = false;
        }
    }
</script>
</body>

</html>
