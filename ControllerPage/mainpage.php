<?php
session_start();
$type = $_SESSION['Type'];
$ID = $_SESSION['ID'];
?>
<html>
<head>
    <script src="../myAjax.js"></script>
    <script type="text/javascript" src="../jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/stylecss.css">
    <script>
        $(document).ready(function(){
            ajax("POST","php/contPageDB.php",
                { // Input data
                }, 1000 // Wait times
                //接收数据
                ,function (xhr) { // If success
                    var str = xhr.responseText;
                    console.log(str);
                    var obj = JSON.parse(str);
                    $("#contInfo").append("<div class='panel panel-default' style='margin:0 auto;width:300px'>"+
                        "<div class='panel-heading'>Your individual information & company information</div>"+
                        "<table class='table' border='1'><tr>"+
                        "<td>User ID: </td>" + "<td>"+ obj.userID + "</td>"+"</tr>"+
                        "<tr><td>User name:</td><td>" + obj.name + "</td></tr>"+
                        "<tr><td>Work age:</td><td>"+ obj.workAge+ "</td></tr>"+
                        "<tr><td>phone:</td><td>"+obj.phone+"</td></tr>"+
                        "<tr><td>Company:</td><td>"+obj.compName+"</td></tr>"+
                        "<tr><td>Juridical:</td><td>"+obj.juri+"</td></tr>"+
                        "<tr><td>Capital:</td><td>"+obj.cap+"</td></tr>"+
                        "<tr><td>Company Email:</td><td>"+obj.compMail+"</td></tr>"+
                        "<tr><td>City:</td><td>"+obj.city+"</td></tr>"+
                        "</table></div>");
                }, function (xhr) { // If fail
                    alert(xhr.status);
                })
        });
    </script>
    <div id="status" style="text-align: right">
        <ul>
            <li><a href="php/cleanSession.php">Log Out</a></li>
            <li><a href="javascript:history.go(-1);">Back</a></li>
            <li><a href="../Mainpage/index.php">Home</a></li>
        </ul>
    </div>
    <h1>Controller Page</h1></head>
<body>

<!--先显示数据，提供修改按钮之后，跳转到修改页面，
更改界面有保存按钮，保存时将信息发至后端，检查后端是否成功后弹出提示框：修改成功！
然后跳回更新后的信息页面-->
<div id="InfoForm">
    <h3 id="contInfo">Controller Info</h3>
</div>
<!--跳回后端用header搞定-->
<div id="modifyForm">
    <form method="post" action="modifyInfo.php">
        <button type="submit" id="modifyBtn" >Modify</button>
    </form>
</div>

<!--给出一个可以input的form，
填写完后，确认按钮，然后发至后端，后端成功后弹出提示框：发布成功！
然后跳回信息界面-->
<div id="postForm">
    <h4>Post recruitment information</h4>
    <input type="button" id="postBtn" onclick="window.location.href='postInfo.php'" value="Post">
</div>

<!--像后端请求检索该公司id的inbox内容，并打印到当前界面-->
<div id="checkInForm">
    <h4>Check Inbox</h4>
    <form method="post" action="Pagination/checkInbox.php">
        <button type="submit" id="goInboxBtn" >Check Inbox</button>
    </form>
</div>

<div id="changePswForm">
    <h4>Change Password</h4>
    <input type="button" id="changePswBtn" onclick="window.location.href='changePsw.php'" value="Change Password">
</div>

</body>
</html>