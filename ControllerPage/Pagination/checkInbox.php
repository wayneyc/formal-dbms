<?php
session_start();
$compID = $_SESSION['CompID'];
//$compID = 1;
?>

<!doctype html>
<html lang="zh-cn">
<head>
    <title>Pagination</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="https://www.jq22.com/jquery/bootstrap-3.3.4.css">
    <script src="https://www.jq22.com/jquery/jquery-1.10.2.js"></script>
    <script src="Pagination.js"></script>
    <script src="../../myAjax.js"></script>
</head>

<body>
<p id="cID" hidden><?php echo $compID?></p>
<h1>Pagination</h1>
<br>
<input type="button" id="postBtn" onclick="window.location.href='../mainpage.php'" value="Back">

<div>
    <div class="myContent"></div>
</div>

<div class="container">
    <div class="myPagination"></div>
</div>

<ul id="insertInfo" class="list-group">
</ul>

<script>
    var compID = document.getElementById('cID').innerText;
    console.log(compID);
    var total;
    show();
    ajax("GET", "Pagination.php", {
        // Input
        "check": true,
        "compID":compID
    }, 100, function (xhr) {
        // Output
        var str = xhr.responseText;
        var obj = JSON.parse(str);
        total = obj.count;
        console.log('Total: ' + total);
        $(".myPagination").Pagination({
            page: 1,
            limit: 20, // the item amount in a page
            count: total,
            groups: 5,
            onPageChange: function (page) {
                deleted();
                show(page,this.limit);
                console.log("Here is:" + page);
            }
        });
        // console.log(total);
    }, function (xhr) {
        console.log("Get total FAIL! " + xhr.state());
    })

    function show(curr_page, page_size) {
        curr_page = curr_page || 1;
        page_size = page_size || 10; // remember to change
        // Initialization
        ajax("GET","Pagination.php",{
            // Input
            "compID":compID
        },1000,function (xhr) {
            // Output
            var str = xhr.responseText;
            var obj = JSON.parse(str);
            console.log(str);
            var info = obj.info;
            var textinfo = '<table class="newNode"><tr><th>UID</th><th>Description</th><th>Interest</th><th>School</th><th>Languages</th><th>Certificate</th></tr>';
            for (var i=(curr_page-1)*page_size; (i<curr_page*page_size)&&(i<total); i++) {
                // Here to change the context of the page
                textinfo += '<tr>' +
                    '<td>' + info[i].UID + '</td>' +
                    '<td>' + info[i].Description + '</td>' +
                    '<td>' + info[i].Interest + '</td>' +
                    '<td>' + info[i].School + '</td>' +
                    '<td>' + info[i].Languages + '</td>' +
                    '<td>' + info[i].Certificate + '</td>' +
                    '</tr>';
            }
            textinfo += '</table>';
            $("#insertInfo").append(textinfo);
            console.log(info);
            // count
        },function (xhr) {
            console.log("Get total FAIL!");
        })
    }

    function deleted() {
        $(".newNode").remove();
    }

    function sleep(d){
        for(var t = Date.now();Date.now() - t <= d;);
    }

</script>

</body>
</html>
