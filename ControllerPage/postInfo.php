<?php
session_start();
$type = $_SESSION['Type'];
$ID = $_SESSION['ID'];
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/stylecss.css">
</head>
<body>
<div id="status" style="text-align: right">
    <ul>
        <li><a href="php/cleanSession.php">Log Out</a></li>
        <li><a href="javascript:history.go(-1);">Back</a></li>
        <li><a href="../Mainpage/index.php">Home</a></li>
    </ul>
</div>
<h1>Post recruitment information</h1>
<form method="post" action="php/postInfoDB.php">
    <p>
        Position:
        <input type="text" name="position" id="position"  />
    </p>
    <p>
        Wage:
        <input type="number" name="wage" id="wage"/>
    </p>
    <p>
        Description:
        <input type="text" name="description" id="description"/>
    </p>
    <p>
        Set Date:
        <input type="date" name="setD" id="setD"/>
    </p>
    <p>
        City:
        <input type="text" name="city" id="city"/>
    </p>
    <button type="submit" id="postBtn" disabled>Post</button>
</form>
<script type="text/javascript">
    document.getElementById("position").addEventListener("input",checkNull);
    document.getElementById("wage").addEventListener("input",checkNull);
    document.getElementById("description").addEventListener("input",checkNull);
    document.getElementById("setD").addEventListener("input",checkNull);
    document.getElementById("city").addEventListener("input",checkNull);

    function checkNull() {
        var pos = document.getElementById("position").value;
        var wage = document.getElementById("wage").value;
        var des = document.getElementById("description").value;
        var setD = document.getElementById("setD").value;
        var city = document.getElementById("city").value;
        if((pos != "")&&(wage != "")&&(des != "")&&(setD != "")&&(city != "")){
            document.getElementById("postBtn").disabled = false;
        }else{
            document.getElementById("postBtn").disabled = true;
        }
    }
</script>
</body></html>