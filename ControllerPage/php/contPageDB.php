<?php
session_start();
$ID =  $_SESSION['ID'];

// Connect to DB
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dbms_test";

// Json class
class controller {
    public $status = 0;
    public $msg = '';
    public $userID = '';
    public $name = '';
    public $workAge = '';
    public $phone = '';
    public $compName = '';
    public $juri = '';
    public $cap = '';
    public $compMail = '';
    public $city = '';
}

// Connect
$conn = new mysqli($servername, $username, $password, $dbname);

// Check
if ($conn->connect_error) {
    die("Fail: " . $conn->connect_error . "<br>");
}

// sql controller info
$sql_p = "
SELECT P.Name as name, P.Phone as phone, C.CompID as compID, C.WorkAge as workAge
FROM People as P, Controllers as C
WHERE P.ID = C.ID and C.ID = $ID
";
$result = $conn->query($sql_p);

$compID = 0;
$contInfo = new controller();
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $compID = $row['compID'];
    $contInfo->userID = $ID;
    $contInfo->name = $row["name"];
    $contInfo->workAge = $row["workAge"];
    $contInfo->phone = $row["phone"];

    // sql company info
    $sql_c = "
    SELECT CompName, Juridical, Capital, CompEmail, City
    FROM Companies
    WHERE CompID = $compID
    ";
    $result_comp = $conn->query($sql_c);

    if ($result_comp->num_rows > 0) {
        $row_comp = $result_comp->fetch_assoc();
        $contInfo->compName = $row_comp['CompName'];
        $contInfo->juri = $row_comp['Juridical'];
        $contInfo->cap = $row_comp['Capital'];
        $contInfo->compMail = $row_comp['CompEmail'];
        $contInfo->city = $row_comp['City'];
        // Save CompID
        $_SESSION['CompID'] = $compID;
    } else {
        $contInfo->status = 1;
        $contInfo->msg = 'No his or her company find!';
    }
} else {
    $contInfo->status = 1;
    $contInfo->msg = 'No Controller find!';
}

// Return
echo json_encode($contInfo);