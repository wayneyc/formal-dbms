<?php
session_start();
$type = $_SESSION['Type'];
$ID = $_SESSION['ID'];
?>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/stylecss.css">
    <div id="status" style="text-align: right">
        <ul>
            <li><a href="php/cleanSession.php">Log Out</a></li>
            <li><a href="javascript:history.go(-1);">Back</a></li>
            <li><a href="../Mainpage/index.php">Home</a></li>
        </ul>
    </div>
    <h1>Modify Info</h1></head>
<body>
<!--controller 在表里的数据几乎都是系统分配的，并不能更改，主要是对公司信息进行更改-->
<!--这个直接提交表单就行了吧-->
<form method="post" action="php/modifyInfoDB.php">
    <p>
        Juridical:
        <input type="text" name="juridical" id="juridical"/>
    </p>
    <p>
        Capital:
        <input type="number" name="capital" id="capital"/>
    </p>
    <p>
        Company Email:
        <input type="email" name="compmail" id="compmail"/>
    </p>
    <p>
        City:
        <input type="text" name="city" id="city"/>
    </p>
    <button type="submit" id="confirmBtn" disabled>Submit</button>
</form>

<script type="text/javascript">
    document.getElementById("juridical").addEventListener("input",checkNull);
    document.getElementById("capital").addEventListener("input",checkNull);
    document.getElementById("compmail").addEventListener("input",checkNull);
    document.getElementById("city").addEventListener("input",checkNull);


    function checkNull() {
        var jur = document.getElementById("juridical").value;
        var cap = document.getElementById("capital").value;
        var mail = document.getElementById("compmail").value;
        var city = document.getElementById("city").value;
        if((jur != "")&&(cap != "")&&(mail != "")&&(city != "")){
            document.getElementById("confirmBtn").disabled = false;
        }else{
            document.getElementById("confirmBtn").disabled = true;
        }
    }
</script>
</body>
</html>