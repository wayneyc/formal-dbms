<?php
session_start();
if (!isset($_SESSION['Type'])) {
    $_SESSION['Type']='';
} else {
    $type = $_SESSION['Type'];
}
if (!isset($_SESSION['ID'])) {
    $_SESSION['ID']='';
} else {
    $ID = $_SESSION['ID'];
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>search page</title>
    <meta name="keywords" content="页面关键字" />
    <meta name="description" content="页面描述" />
    <link href="https://at.alicdn.com/t/font_1551254_rxrrzgz2kjc.css" rel="stylesheet" type="text/css" />
    <link href="http://src.axui.cn/src/css/ax.css" rel="stylesheet" type="text/css">
    <link href="http://src.axui.cn/src/css/ax-response.css" rel="stylesheet" type="text/css">
    <link rel='stylesheet' type='text/css' href='css/searchPage.css' />
</head>

<body>

    <div class="loginState ax-bg-info ax-color-warning">
        <!-- 顶部登录栏 -->
        <p id="type" hidden><?php echo $type ?>
        </p>
        <p id="ID" hidden><?php echo $ID ?>
        </p>
        <script>
            var type = document.getElementById('type').innerText;
            var id = document.getElementById('ID').innerText;
            console.log('Search,Type: ' + type);
            console.log('Search,ID: ' + id);
        </script>
        <div>
            <?php
    // http://localhost/Beta/for-php/formal_edition/
    // 不同身份进入不同主页
    if ($type == 'User') {
        echo '
        <a href="../UserPage/mainpage.php">User Page  </a>
        ';
    } elseif ($type == 'Boss') {
        echo '
        <a href="../BossPage/mainpage.php">Boss Page  </a>
        ';
    } elseif ($type == 'Controller') {
        echo '
        <a href="../ControllerPage/mainpage.php">Controller Page  </a>
        ';
    } else {
        echo '
        <a href="LR.php">Login & Register</a>
        ';
    }
    // 若有登录，则有登出选项
    if ($type != '') {
        echo '<a href="php/cleanSession.php">Log out</a>';
    }
    echo '<a href="../Mainpage/index.php">Home</a></li>';
    ?>
        </div>
    </div>

    <div class="searchType ax-radius-left ax-radius-right ">
        <!--选择搜索类型：搜公司还是职位-->
        <div id="findCompany" class="ax-radius-left">
            <button class=" ax-btn">Find Companies</button>
        </div>
        <div id="findPosition" class="ax-radius-right">
            <button class="ax-btn">Find Positions</button>
        </div>
    </div>

    <!--搜索公司选项-->
    <div id="searchForCompany">
        <!--搜索输入框-->
        <div class="inputBox">
            <br />
            <input type="text" name="companyName" placeholder="company name..." id="companyName" />
        </div>
        <!--搜索按钮-->
        <div class="submitButtonBox">
            <br />
            <input type='submit' value='Search Company' class="submitButton ax-btn ax-primary" name="searchCompany"
                id="searchCompany" onclick='searchCompany()' />
        </div>
        <!--分页显示-->
        <br><br><br>
        <div class="container">
            <div class="myPagination"></div>
        </div>
        <div id="insertInfo">
        </div>
    </div>

    <!--搜索职位选项-->
    <div id="searchForPosition">
        <!--搜索职位-->
        <div class="inputBox">
            <br>
            <input type="text" name="positionName" placeholder="Position name..." id="positionName" />
        </div>
        <!--搜索按钮-->
        <div class="submitButtonBox">
            <br />
            <input type='submit' value='Search Position' class="submitButton ax-btn ax-primary" name="searchPosition"
                id="searchPosition" onclick="searchPosition()" />
        </div>
        <br><br><br>
        <!--选择城市-->
        <div id="selectMenuLeft">
            <span>Working City</span>
            <select class="ax-select" name="selectCity" id="selectCity">
                <option value="" selected>Please select...</option>
                <option value="Shenzhen">Shenzhen</option>
                <option value="NewYork">NewYork</option>
                <option value="Paris">Paris</option>
                <option value="Singapore">Singapore</option>
                <option value="Tokyo">Tokyo</option>
                <option value="Shanghai">Shanghai</option>
                <option value="Beijin">Beijin</option>
                <option value="HongKong">HongKong</option>
                <option value="London">London</option>
                <option value="Guangzhou">Guangzhou</option>
            </select>
        </div>
        <!--选择月薪-->
        <div id="selectMenuRight">
            <span>Monthly Pay</span>
            <select class="ax-select" name="selectWage" id="selectWage">
                <option value="" selected>Please select...</option>
                <option value="3000">less than 4,000</option>
                <option value="5000">4,000-6,000</option>
                <option value="7000">6,000-8,000</option>
                <option value="9000">8,000-10,000</option>
                <option value="11000">10,000-12,000</option>
                <option value="13000">12,000-14,000</option>
                <option value="15000">14,000-16,000</option>
                <option value="17000">16,000-18,000</option>
                <option value="19000">more than 18,000</option>
            </select>
        </div>
        <!--分页显示-->
        <br><br><br>
        <div class="container">
            <div class="myPagination2">
            </div>
        </div>
        <div id="table">
            <div id="insertInfo2">
            </div>
        </div>
    </div>

    <script src="http://src.axui.cn/src/js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://src.axui.cn/src/js/ax.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://www.jq22.com/jquery/bootstrap-3.3.4.css">
    <script src="https://www.jq22.com/jquery/jquery-1.10.2.js"></script>
    <script src="Pagination/Pagination.js"></script>
    <script src="../myAjax.js"></script>
    <script type='text/javascript'>
        hideForms();

        document.getElementById("findCompany").addEventListener("click", function() {
            showForm("searchForCompany");
        });
        document.getElementById("findPosition").addEventListener("click", function() {
            showForm("searchForPosition");
        });

        function hideForms() {
            document.getElementById("searchForCompany").style.display = "none";
            document.getElementById("searchForPosition").style.display = "none";
        }

        function showForm(id) {
            hideForms();
            document.getElementById(id).style.display = "block";
            document.getElementById(id).style.visibility = "visible";
        }
        showForm("searchForCompany");

        function searchCompany() {
            var companyName = document.getElementById("companyName").value;
            ajax("GET", "Pagination/searchCompDB.php", {
                // Input
                "compName": companyName
            }, 100, function(xhr) {
                // Output
                var limit = 20;
                var str = xhr.responseText;
                var obj = JSON.parse(str);
                var count = obj.count;
                console.log('Search Company');
                console.log(obj);
                deleted();
                show(1, limit, count, obj);
                $(".myPagination").Pagination({
                    page: 1,
                    limit: limit, // the item amount in a page
                    count: count,
                    groups: 5,
                    onPageChange: function(page) {
                        deleted();
                        show(page, this.limit, count, obj);
                        console.log("Here is:" + page);
                    }
                });
            }, function(xhr) {
                console.log("Get total FAIL! " + xhr.state());
            })
            console.log(companyName);
            //获得公司名字
        }

        function searchPosition() {
            var positionName = document.getElementById("positionName").value;
            console.log(positionName);
            //获得职位
            var selectWage = document.getElementById("selectWage").value;
            console.log(selectWage);
            //获得薪水范围
            var selectCity = document.getElementById("selectCity").value;
            console.log(selectCity);
            //获得工作城市
            ajax("GET", "Pagination/searchJobDB.php", {
                // Input
                "positionName": positionName,
                "selectWage": selectWage,
                "selectCity": selectCity
            }, 100, function(xhr) {
                // Output
                var limit = 20;
                var str = xhr.responseText;
                var obj = JSON.parse(str);
                var count = obj.count;
                console.log('Search Job');
                console.log(obj);
                deleted();
                show_2(1, limit, count, obj);
                $(".myPagination2").Pagination({
                    page: 1,
                    limit: limit, // the item amount in a page
                    count: count,
                    groups: 5,
                    onPageChange: function(page) {
                        deleted();
                        show_2(page, this.limit, count, obj);
                        // console.log("Here is:" + page);
                    }
                });
            }, function(xhr) {
                console.log("Get total FAIL! " + xhr.state());
            })
        }

        function show(curr_page, page_size, amount, obj) {
            curr_page = curr_page || 1;
            page_size = page_size || 10; // remember to change
            var max_page = Math.ceil(amount / page_size);
            // start_index & end_index
            var s_index = (curr_page - 1) * page_size;
            var e_index = curr_page * page_size - 1;
            if (curr_page === max_page) {
                e_index = amount - 1;
            }
            var info = obj.info;
            var textinfo =
                '<table class="newNode" border="1" cellpadding="20"><tr><th>CompName</th><th>Boss</th><th>Juridical</th><th>Capital</th><th>CompEmail</th><th>BuildDate</th><th>City</th></tr>';
            for (var i = s_index; i <= e_index; i++) {
                // Here to change the context of the page
                textinfo +=
                    '<tr>' +
                    '<td>' + info[i].CompName + '</td>' +
                    '<td>' + info[i].Boss + '</td>' +
                    '<td>' + info[i].Juridical + '</td>' +
                    '<td>' + info[i].Capital + '</td>' +
                    '<td>' + info[i].CompEmail + '</td>' +
                    '<td>' + info[i].BuildDate + '</td>' +
                    '<td>' + info[i].City + '</td>' +
                    '</tr>';
            }
            textinfo += '</table>';

            $("#insertInfo").append(textinfo);

        }

        function show_2(curr_page, page_size, amount, obj) {
            curr_page = curr_page || 1;
            page_size = page_size || 10; // remember to change
            var max_page = Math.ceil(amount / page_size);
            // start_index & end_index
            var s_index = (curr_page - 1) * page_size;
            var e_index = curr_page * page_size - 1;
            if (curr_page === max_page) {
                e_index = amount - 1;
            }
            var info = obj.info;
            var textinfo =
                '<table class="newNode" border="1" cellpadding="20"><tr><th>CompID</th><th>Position</th><th>Wage</th><th>Description</th><th>SetDate</th><th>City</th></tr>';
            for (var i = s_index; i <= e_index; i++) {
                // Here to change the context of the page
                textinfo +=
                    '<tr>' +
                    '<td>' + info[i].CompID + '</td>' +
                    '<td>' + info[i].Position + '</td>' +
                    '<td>' + info[i].Wage + '</td>' +
                    '<td>' + info[i].Description + '</td>' +
                    '<td>' + info[i].SetDate + '</td>' +
                    '<td>' + info[i].City + '</td>' +
                    '</tr>';
            }
            textinfo += '</table>';
            $("#insertInfo2").append(textinfo);

        }

        function deleted() {
            $(".newNode").remove();
        }

        function sleep(d) {
            for (var t = Date.now(); Date.now() - t <= d;);
        }
    </script>
</body>

</html>