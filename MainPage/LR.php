<!DOCTYPE html>
<?php session_start();
$_SESSION['ID']="";
$_SESSION['Type']=""?>
<html>

<head>
    <meta charset="utf-8" />
    <title>welcome to our website </title>
    <meta name="keywords" content="页面关键字" />
    <meta name="description" content="页面描述" />
    <link href="http://at.alicdn.com/t/font_1551254_rxrrzgz2kjc.css" rel="stylesheet" type="text/css" />
    <link href="http://src.axui.cn/src/css/ax.css" rel="stylesheet" type="text/css" />
    <link href="http://src.axui.cn/src/css/ax-response.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type='text/css' href='css/loginOrRegister.css' />
</head>

<body>
    <div class="welcome ax-radius-md ax-bg-primary ax-gradient-315">
        <h1><br>Welcome to DB oasis </h1>
    </div>
    <div id="serachutton">
    <button id="postBtn" onclick="window.location.href='index.php'" class="ax-btn ax-primary ax-gradient ">search</button>
    </div>
    <div class="someProfession ax-radius-md demo-section ax-shadow ax-border">
        <div id="left"><button class="ax-btn">boss</button></div>
        <div id="center"><button class="ax-btn">user</button></div>
        <div id="right"><button class="ax-btn">controller</button></div>
    </div>
    <div id="boss" class="ax-radius-md demo-section ax-shadow-dark ax-border">
        <form method="post" action="login.php">
            <input type="submit" value="bossLogin" class="ax-btn ax-line ax-primary" name="bossLogin">
        </form>
        <form method="post" action="register.php">
            <input type="submit" value="bossRegister" class="ax-btn ax-line ax-primary" name="bossRegister">
        </form>
    </div>
    <div id="user" class="ax-radius-md demo-section ax-shadow-dark ax-border">
        <form method="post" action="login.php">
            <input type="submit" value="userLogin" class="ax-btn ax-line ax-ad" name="userLogin">
        </form>
        <form method="post" action="register.php">
            <input type="submit" value="userRegister" class="ax-btn ax-line ax-ad" name="userRegister">
        </form>
    </div>
    <div id="controller" class="ax-radius-md demo-section ax-shadow-dark ax-border">
        <form method="post" action="login.php">
            <input type="submit" value="controllerLogin" class="ax-btn ax-line ax-secondary" name="controllerLogin">
        </form>
        <form method="post" action="register.php">
            <input type="submit" value="controllerRegister" class="ax-btn ax-line ax-secondary" name="controllerRegister">
        </form>
        </form>
    </div>

    <script src="http://src.axui.cn/src/js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://src.axui.cn/src/js/ax.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //js内容
        });

        function hideForms() {
            document.getElementById("boss").style.display = "none";
            document.getElementById("user").style.display = "none";
            document.getElementById("controller").style.display = "none";
        }
        hideForms();
        document.getElementById("left").addEventListener("click", function () { showForm("boss"); });
        document.getElementById("center").addEventListener("click", function () { showForm("user"); });
        document.getElementById("right").addEventListener("click", function () { showForm("controller"); });

        function showForm(id) {
            hideForms();
            document.getElementById(id).style.display = "block";
            document.getElementById(id).style.visibility = "visible";
        }

    </script>
</body>

</html>