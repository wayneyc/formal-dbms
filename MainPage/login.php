<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>login</title>
    <meta name="keywords" content="页面关键字" />
    <meta name="description" content="页面描述" />
    <link href="http://at.alicdn.com/t/font_1551254_rxrrzgz2kjc.css" rel="stylesheet" type="text/css" />
    <link href="http://src.axui.cn/src/css/ax.css" rel="stylesheet" type="text/css">
    <link href="http://src.axui.cn/src/css/ax-response.css" rel="stylesheet" type="text/css">

</head>

<body>
    <?php
    $case=0;
    if (@$_POST['bossLogin']) {
        $case=1;
    } elseif (@$_POST['userLogin']) {
        $case=2;
    } elseif (@$_POST['controllerLogin']) {
        $case=3;
    }
    ?>
    <div class="welcome ax-radius-md ax-bg-primary ax-gradient-315">
        <div>Login</div>
    </div>
    <?php
    echo"<link rel='stylesheet' type='text/css' href='css/login.css' />";
    if ($case==1) { //老板登录
        echo"
        <form method='post' action='php/loginDB_boss.php'>
        <div class='login ax-radius-md'>
        <div>
        Phone Number
        <input type='text' name='bossAccount' id='bossAccount'>
        </div>

        <div>
        Password
        <input type='password' name='bossPassword' id='bossPassword'>
        </div>

        <div>
        <input type='submit' value='Boss Login' name='bossLogin' id='bossLogin' onclick='getBossInfo()' class='ax-btn ax-primary'>
        </div>

        </form>";
    } elseif ($case==2) { //应聘者登录
        echo"
        <form method='post' action='php/loginDB_user.php'>
        <div class='login ax-radius-md'>
        <div>
        Phone Number
        <input type='text' name='userAccount' id='userAccount'>
        </div>

        <div>
        Password
        <input type='password' name='userPassword' id='userPassword'>
        </div>

        <div>
        <input type='submit' value='User Login' name='userLogin' id='userLogin' onclick='getUserInfo()' class='ax-btn ax-primary'>
        </div>

        </form>";
    } elseif ($case==3) { //管理者登录
        echo"
        <form method='post' action='php/loginDB_cont.php'>
        <div class='login ax-radius-md'>
        <div>
        Phone Number
        <input type='text' name='controllerAccount' id='controllerAccount'>
        </div>

        <div>
        Password
        <input type='password' name='controllerPassword' id='controllerPassword'>
        </div>

        <div>
        <input type='submit' value='Controller Login' name='controllerLogin' id='controllerLogin'
        onclick='getControllerInfo()' class='ax-btn ax-primary '>
        </div>

        </form>";
    }
    ?>

    <script src="http://src.axui.cn/src/js/jquery-1.10.2.min.js" type="text/javascript">
    </script>
    <script src="http://src.axui.cn/src/js/ax.min.js" type="text/javascript"></script>
    <script src="base64.js">
        //本地的base64.js文件，来自https://github.com/dankogai/js-base64
    </script>
    <script type='text/javascript'>
        function getBossInfo() {
            var bossAccount = document.getElementById("bossAccount").value;
            bossAccount = Base64.encode(bossAccount);
            console.log(bossAccount);
            //获得老板的电话号码并进行编码
            var bossPassword = document.getElementById("bossPassword").value;
            bossPassword = Base64.encode(bossPassword);
            console.log(bossPassword);
            //获得老板的密码并进行编码
        };

        function getUserInfo() {
            var userAccount = document.getElementById("userAccount").value;
            userAccount = Base64.encode(userAccount);
            console.log(userAccount);
            //获得应聘者的电话号码并编码
            var userPassword = document.getElementById("userPassword").value;
            userPassword = Base64.encode(userPassword);
            console.log(userPassword);
            //获得应聘者的密码并进行编码
        };

        function getControllerInfo() {
            var controllerAccount = document.getElementById("controllerAccount").value;
            controllerAccount = Base64.encode(controllerAccount);
            console.log(controllerAccount);
            //获得管理者的电话号码并编码
            var controllerPassword = document.getElementById("controllerPassword").value;
            controllerPassword = Base64.encode(controllerPassword);
            console.log(controllerPassword);
            //获得管理者的密码并编码
        };
    </script>
</body>

</html>