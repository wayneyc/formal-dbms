<?php
$positionName = $_GET['positionName'];
$selectPosition = $_GET['selectPosition'];
$selectWage = ($_GET['selectWage'] != '') ? $_GET['selectWage'] : 10000;
$selectCity = $_GET['selectCity'];

// Connect to DB
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dbms_test";

// Connect
$conn = new mysqli($servername, $username, $password, $dbname);

// Check
if ($conn->connect_error) {
    die("Connect fail: " . $conn->connect_error . "<br>");
}

// Sql
$sql = "
SELECT *
FROM Jobs
WHERE (Position like '%$positionName%') and (Position like '%$selectPosition%') and (City like '%$selectCity%') and (Wage between ($selectWage - 1000) and ($selectWage + 1000) )
";
// Position like '%$positionName%' and City like '%$selectCity%' and
$result = $conn->query($sql);
$count = $result->num_rows;
$num = 0;
$info = "[";
while($row = $result->fetch_assoc()) {
    $info .= '{' . '"CompID":"' . $row["CompID"] . '", "Position":"' . $row["Position"] . '", "Wage":"' . $row["Wage"] . '", "Description":"' . $row["Description"] . '", "SetDate":"' . $row["SetDate"] . '", "City":"' . $row["City"] . '"}';
    $num += 1;
    if ($num != $count) {
        $info .= ',';
    }
}
$info .= ']';
$ret = '{"count":"' . $count . '","info":' . $info . '}';
echo $ret;
