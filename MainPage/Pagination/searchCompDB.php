<?php

$compName = $_GET['compName'];

// Connect to DB
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "dbms_test";

// Connect
$conn = new mysqli($servername, $username, $password, $dbname);

// Check
if ($conn->connect_error) {
    die("Connect fail: " . $conn->connect_error . "<br>");
}

$sql = "
SELECT *
FROM Companies
WHERE CompName like '%$compName%'
";
$result = $conn->query($sql);
$count = $result->num_rows;
$num = 0;
$info = "[";
while($row = $result->fetch_assoc()) {
    $info .= '{' . '"CompName":"' . $row["CompName"] . '", "Boss":"' . $row["BossID"] . '", "Juridical":"' . $row["Juridical"] . '", "Capital":"' . $row["Capital"] . '", "CompEmail":"' . $row["CompEmail"] . '", "BuildDate":"' . $row["BuildDate"] . '", "City":"' . $row["City"] . '"}';
    $num += 1;
    if ($num != $count) {
        $info .= ',';
    }
}
$info .= ']';
$ret = '{"count":"' . $count . '","info":' . $info . '}';
echo $ret;