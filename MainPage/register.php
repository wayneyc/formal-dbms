<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Register</title>
    <meta name="keywords" content="页面关键字" />
    <meta name="description" content="页面描述" />
    <link href="http://at.alicdn.com/t/font_1551254_rxrrzgz2kjc.css" rel="stylesheet" type="text/css" />
    <link href="http://src.axui.cn/src/css/ax.css" rel="stylesheet" type="text/css">
    <link href="http://src.axui.cn/src/css/ax-response.css" rel="stylesheet" type="text/css">
</head>

<body>
    <?php
    $case=0;
    if (@$_POST['bossRegister']) {
        $case=1;
    } elseif (@$_POST['userRegister']) {
        $case=2;
    } elseif (@$_POST['controllerRegister']) {
        $case=3;
    }
    ?>
    <div class="welcome ax-radius-md ax-bg-primary ax-gradient-315">
        <div>Register</div>
    </div>
    <?php
    echo"<link rel='stylesheet' type='text/css' href='css/register.css' />";
    if ($case==1) { //老板注册
        echo"

        <form action='php/registerDB_boss.php' method='post'>
        <div class='register ax-radius-md'>



        <div class='row' >
        Name*<b id='alertName' class='alert'></b>
        <br />
        <input name='Name' id='Name' type='text'>
        </div>

        <div class='row' >
        Password*<b id='alertPassword' class='alert'></b>
        <br />
        <input name='Password' id='Password' type='password'>
        </div>

        <div class='row'>
        Gender
        <br/>
        <select name='gender' id='gender'>
            <option value='0' selected>male</option>
            <option value='1'>female</option>
        </select>
        </div>

        <div class='row'>
        phone number*<b id='alertPhoneNumber' class='alert'></b>
        <br/>
        <input name='PhoneNumber' id='PhoneNumber' type='tel'>
        </div>

        <div class='row'>
        E-mail*<b id='alertE-mail' class='alert'></b>
        <br/>
        <input name='E-mail' id='E-mail' type='email'>
        </div>

        <div class='row'>
        <br/>
        <input type='submit' value='Boss Register' id='bossRegister' onclick='getBossInfo()' class='ax-btn ax-primary' >
        </div>


        </div>
        </form>";
    } elseif ($case==2) { //应聘者注册
        echo"

        <form action='php/registerDB_user.php' method='post'>

        <div class='register ax-radius-md'>

        <div class='row' >
        Name*<b id='alertName' class='alert'></b>
        <br />
        <input name='Name' id='Name' type='text'>
        </div>

        <div class='row' >
        Password*<b id='alertPassword' class='alert'></b>
        <br />
        <input name='Password' id='Password' type='password'>
        </div>

        <div class='row'>
        Gender
        <br/>
        <select name='gender' id='gender'>
            <option value='0' selected>male</option>
            <option value='1'>female</option>
        </select>
        </div>

        <div class='row'>
        phone number*<b id='alertPhoneNumber' class='alert'></b>
        <br/>
        <input name='PhoneNumber' id='PhoneNumber' type='tel'>
        </div>

        <div class='row'>
        E-mail*<b id='alertE-mail' class='alert'></b>
        <br/>
        <input name='E-mail' id='E-mail' type='email'>
        </div>

        <div class='row'>
        Birthday*<b id='alertBirthday' class='alert'></b>
        <br/>
        <input name='Birthday' id='Birthday' type='date'>
        </div>

        <div class='row'>
        Education*<b id='alertEducation' class='alert'></b>
        <br />
        <input name='Education' id='Education' type='text'>
        </div>

        <div class='row'>
        <br/>
        <input type='submit' value='userRegister' id='userRegister' onclick='getUserInfo()' class='ax-btn ax-primary'>
        </div>

        </div>

        </form>
        ";
    } elseif ($case==3) { //管理者注册
        echo"
        <form action='php/registerDB_cont.php' method='post'>

        <div class='register ax-radius-md'>

        <div class='row' >
        Name*<b id='alertName' class='alert'></b>
        <br />
        <input name='Name' id='Name' type='text'>
        </div>

        <div class='row' >
        Password*<b id='alertPassword' class='alert'></b>
        <br />
        <input name='Password' id='Password' type='password'>
        </div>

        <div class='row' >
        Key*<b id='alertPassword' class='alert'></b>
        <br />
        <input name='Key' id='Key' type='password'>
        </div>

        <div class='row'>
        Gender
        <br/>
        <select name='gender' id='gender'>
            <option value='0' selected>male</option>
            <option value='1'>female</option>
        </select>
        </div>

        <div class='row'>
        phone number*<b id='alertPhoneNumber' class='alert'></b>
        <br/>
        <input name='PhoneNumber' id='PhoneNumber' type='tel'>
        </div>

        <div class='row'>
        E-mail*<b id='alertE-mail' class='alert'></b>
        <br/>
        <input name='E-mail' id='E-mail' type='email'>
        </div>

        <div class='row'>
        CompID*<b id='alertCompID' class='alert'></b>
        <br/>
        <input name='CompID' id='CompID' type='number'>
        </div>

        <div class='row'>
        WorkAge*<b id='alertWorkAge' class='alert'></b>
        <br/>
        <input name='WorkAge' id='WorkAge' type='number'>
        </div>

        <div class='row'>
        <br/>
        <input type='submit' value='controllersRegister' onclick='getControllerInfo()' class='ax-btn ax-primary'>
        </div>
        </div>
        </form>";
    }
    ?>
    <script src="http://src.axui.cn/src/js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://src.axui.cn/src/js/ax.min.js" type="text/javascript"></script>
    <script src="base64.js">
        //本地的base64.js文件，来自https://github.com/dankogai/js-base64
    </script>
    <script type='text/javascript'>
        function getBossInfo() {
            var Name = document.getElementById("Name").value;
            // Name = Base64.encode(Name);
            console.log(Name);
            //获得老板名字
            var Password = document.getElementById("Password").value;
            Password = Base64.encode(Password);
            console.log(Password);
            //获得老板密码
            var gender = document.getElementById("gender").value;
            // gender = Base64.encode(gender);
            console.log(gender);
            //获得老板性别
            var PhoneNumber = document.getElementById("PhoneNumber").value;
            // PhoneNumber = Base64.encode(PhoneNumber);
            console.log(PhoneNumber);
            //获得老板电话号码
            var Email = document.getElementById("E-mail").value;
            // Email = Base64.encode(Email);
            console.log(Email);
            //获得老板电子邮箱
            var CompanyName = document.getElementById("CompanyName").value;
            // CompanyName = Base64.encode(CompanyName);
            console.log(CompanyName);
            //获得公司名字
            var Juridical = document.getElementById("Juridical").value;
            // Juridical = Base64.encode(Juridical);
            console.log(Juridical);
            //获得公司的Juridical
            var Capital = document.getElementById("Capital").value;
            // Capital = Base64.encode(Capital);
            console.log(Capital);
            //获得公司资金
            var CompEmail = document.getElementById("CompEmail").value;
            // CompEmail = Base64.encode(CompEmail);
            console.log(CompEmail);
            //获得公司的电子邮件
            var BuildDate = document.getElementById("BuildDate").value;
            // BuildDate = Base64.encode(BuildDate);
            console.log(BuildDate);
            //获得公司的建立日期
            var City = document.getElementById("City").value;
            // City = Base64.encode(City);
            console.log(City);
            //获得公司所在城市

        };

        function getUserInfo() {
            var Name = document.getElementById("Name").value;
            // Name = Base64.encode(Name);
            console.log(Name);
            //获得应聘者名字
            var Password = document.getElementById("Password").value;
            Password = Base64.encode(Password);
            console.log(Password);
            //获得应聘者密码
            var gender = document.getElementById("gender").value;
            // gender = Base64.encode(gender);
            console.log(gender);
            //获得应聘者性别
            var PhoneNumber = document.getElementById("PhoneNumber").value;
            // PhoneNumber = Base64.encode(PhoneNumber);
            console.log(PhoneNumber);
            //获得应聘者电话号码
            var Email = document.getElementById("E-mail").value;
            // Email = Base64.encode(Email);
            console.log(Email);
            //获得应聘者电子邮箱
            var Photo = document.getElementById("Photo").value;
            // Photo = Base64.encode(Photo);
            console.log(Photo);
            //获得应聘者的照片
            var Birthday = document.getElementById("Birthday").value;
            // Birthday = Base64.encode(Birthday);
            console.log(Birthday);
            //获得应聘者的生日日期
            var Education = document.getElementById("Education").value;
            // Education = Base64.encode(Education);
            console.log(Education);
            //获得应聘者的学历

        };

        function getControllerInfo() {
            var Name = document.getElementById("Name").value;
            // Name = Base64.encode(Name);
            console.log(Name);
            //获得管理者名字
            var Password = document.getElementById("Password").value;
            Password = Base64.encode(Password);
            console.log(Password);
            //获得管理者密码
            var gender = document.getElementById("gender").value;
            // gender = Base64.encode(gender);
            console.log(gender);
            //获得管理者性别
            var PhoneNumber = document.getElementById("PhoneNumber").value;
            // PhoneNumber = Base64.encode(PhoneNumber);
            console.log(PhoneNumber);
            //获得管理者电话号码
            var Email = document.getElementById("E-mail").value;
            // Email = Base64.encode(Email);
            console.log(Email);
            //获得管理者电子邮箱
            var CompID = document.getElementById("CompID").value;
            // CompID = Base64.encode(CompID);
            console.log(CompID);
            //获得管理者对应的CompID
            var WorkAge = document.getElementById("WorkAge").value;
            // WorkAge = Base64.encode(WorkAge);
            console.log(WorkAge);
            //获得管理者的工作时间

            $(document).ready(function() {
                // console.log("name: " + $("#name").val());
                // url?key:value&key:value...
                ajax("POST", "php/registerDB_cont.php", // DBfile name
                    { // Input data
                        // "name":$("#name").val(),
                        // "id":2
                    }, 1000 // Wait times
                    ,
                    function(xhr) { // If success
                        var str = xhr.responseText;
                        var obj = JSON.parse(str);
                        console.log(obj.name);
                        console.log(obj.date);
                        $("table").append("<tr>" +
                            "<td>name: </td>" + "<td>" + obj.name + "</td>" + "</tr>" +
                            "<tr><td>gender:</td><td>" + obj.gender + "</td></tr>" +
                            "<tr><td>birthday:</td><td>" + obj.birthday + "</td></tr>" +
                            "<tr><td>education:</td><td>" + obj.education + "</td></tr>" +
                            "<tr><td>phone:</td><td>" + obj.phone + "</td></tr>" +
                            "<tr><td>email</td><td>" + obj.email + "</td></tr>");
                    },
                    function(xhr) { // If fail
                        alert(xhr.status);
                    })
            });

        };

        function imgPreview(fileDom, i) {
            if (window.FileReader) {
                var reader = new FileReader();
            } else {
                alert("您的设备不支持图片预览功能，如需该功能请升级您的设备！");
            }
            var file = fileDom.files[0];
            var imageType = /^image\//;
            if (!imageType.test(file.type)) {
                alert("请选择图片！");
                return;
            }
            reader.onload = function(e) {
                //图片路径设置为读取的图片
                // img.src = e.target.result;
                console.log(document.getElementsByClassName('file-box'));
                document.getElementsByClassName('file-box')[i].style.background = "url(" + e.target.result +
                    ")no-repeat"; //回显图片
                document.getElementsByClassName('file-box')[i].style.backgroundSize = '200px 160px';
                console.log('reader', reader)
            };
            reader.readAsDataURL(file);
        }
    </script>
</body>

</html>


<!--<div class='rowb'>-->
<!--    Photo*<b id='alertPhoto' class='alert'></b>-->
<!--    <br/>-->
<!--    <input name='Photo' id='Photo' type='file' value='choose photo'-->
<!--           accept='image/gif,image/jpeg,image/jpg,image/png,image/svg'-->
<!--           placeholder='please choose a photo'-->
<!--           onchange='imgPreview(this,0)'>-->
<!--    <br/>-->
<!--</div>-->