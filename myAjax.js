function obj2str(obj) {
    obj.t = new Date().getTime();
    var res = [];
    for(var key in obj) {
        res.push(key+"="+obj[key]);
    }
    return res.join("&");
}
function ajax(type, url, obj, timeout, success, error) {
    // encodeURIComponent() Chinese2English
    var str = obj2str(obj); // url?key:value&key:value...
    var xmlhttp, timer;
    if (window.XMLHttpRequest) {
        //  IE7+, Firefox, Chrome, Opera, Safari 浏览器执行代码
        xmlhttp = new XMLHttpRequest();
    } else {
        // IE6, IE5 浏览器执行代码
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (type === 'GET') {
        xmlhttp.open("GET", url+"?"+str, true);
        xmlhttp.send();
        console.log("GET");
    } else {
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send(str);
        console.log("POST");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4) {
            clearInterval(timer); // Clean the timer
            if (xmlhttp.status >= 200 && xmlhttp.status < 300 || xmlhttp.status === 304) {
                // console.log(ev2);
                success(xmlhttp);
            } else {
                // console.log("Fail");
                error(xmlhttp);
            }
        }
    }
    if (timeout) {
        timer = setInterval(function () {
            console.log("Ending the require");
            xmlhttp.abort(); // End the require
            clearInterval(timer); // Clean the timer
        }, timeout);
    }
}